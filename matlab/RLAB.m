close all;
dts=0.005;
z=tf('z',dts);
s=tf('s');

N=1000;
t=dts*(0:N-1);
tsim=dts*(N-1);

%FT sistema de velocidad en bucle abierto
% G=0.199407/(z-0.722075); %old motor tf
data = csvread("dataident.csv");


idu=data(1:N,1);
idy=data(1:N,2);


fil=100/(100+s);
idy=lsim(fil,idy,t);

idd0=iddata(idu,idy,dts);
% G=43.75/(s+62.5);
% G=20/(s+28.3);
G=18/(s+25);


step(3*G);
hold on;
plot(t,idy);



% Controlador para el bucle de velocidad
optGv=pidtuneOptions('PhaseMargin',60);
wc=40;
Cv=pidtune(G,'PI',wc,optGv);

ts=0.13;
tp=0.07;
os=0.13;
sig=pi/ts;
wd=pi/tp;
wd=-sig*pi/log(os);
pv=-sig+i*wd;

K=1;
Cv=K*(s+85.25)/s;

%Bucle cerrado en velocidad con controlador PI
Gv=minreal(Cv*G/(1+(Cv*G)));

figure;
step(4*Gv);

