/*BUTTONS*/
var positionB = document.getElementById("positionButton");
var velocityB = document.getElementById("velocityButton");

var positionStatus=false;
var velocityStatus=false;

/*CAMERA*/
var camera_switch = document.getElementById("cameraCheck").checked;
camera_switch = true;

/*LOOP SELECTOR*/

var selectLoop = document.getElementById("loop");

/*RUNNING VARIABLE*/

var running=0;

/*DEFAULT VALUES*/

var kp_value = document.getElementById("kp_value");
var ki_value = document.getElementById("ki_value");
var kd_value = document.getElementById("kd_value");

var sample_time = document.getElementById("sample_time");
var running_time = document.getElementById("running_time");

/*SLIDERS*/

var positionSlider = document.getElementById("positionSlider");
var positionOutput = document.getElementById("demoPosition");

var velocitySlider = document.getElementById("velocitySlider");
var velocityOutput = document.getElementById("demoVelocity");

var dutySlider = document.getElementById("dutySlider");
var dutyOutput = document.getElementById("demoDuty");

/*BUTTON FUNCTON*/
positionB.onclick = function (){
  
  console.log("Pressed");
  positionSlider.value="180";
  velocitySlider.value="0.5";

  document.getElementById("demoPosition").innerHTML = "180";
  document.getElementById("demoVelocity").innerHTML = "3";
  
  positionStatus=!positionStatus;

  if(positionStatus==true){
    positionB.style.backgroundColor="white";
  }
  else{
    positionB.style.backgroundColor="grey";
  }

  if (velocityStatus==true){
    velocityStatus=false;
    velocityB.style.backgroundColor="grey";
  }
}

velocityB.onclick = function (){
  
  console.log("Pressed");
  positionSlider.value="180";
  velocitySlider.value="3";

  document.getElementById("demoPosition").innerHTML = "180";
  document.getElementById("demoVelocity").innerHTML = "3";

  velocityStatus=!velocityStatus;

  if(velocityStatus==true){
    velocityB.style.backgroundColor="white";
  }
  else{
    velocityB.style.backgroundColor="grey";
  }

  if (positionStatus==true){
    positionStatus=false;
    positionB.style.backgroundColor="grey";
  }
}


/*SLIDERS FUNCTION*/

positionOutput.innerHTML = positionSlider.value;
positionSlider.oninput = function() { positionOutput.innerHTML = this.value;}

velocityOutput.innerHTML = velocitySlider.value;
velocitySlider.oninput = function() { velocityOutput.innerHTML = this.value;}

dutyOutput.innerHTML = dutySlider.value;
dutySlider.oninput = function() { dutyOutput.innerHTML = this.value;}


/*GRAPH FUNCTIONS*/

var ctx1= document.getElementById("ChartP_V").getContext("2d");
var ctx2= document.getElementById("ChartC").getContext("2d"); 

lineChart1 = new Chart(ctx1, {
    type: 'line',
    data: {datasets:[{label:'Position [º] or Velocity [rad/s] vs time[s]', data:[0]}, 
                     {label: 'Step Input', data:[0]}]},
    options: {responsive:false, scales:{yAxes:[{ticks:{min:0}}]}}
});

lineChart2 = new Chart(ctx2, {
    type: 'line',
    data: {labels:[], datasets:[{label:'Control signal', data:[]}]},
    options: {responsive:false}
});
  
function createGraph() {
  
  if (lineChart1!=null){lineChart1.destroy();
      lineChart1 = new Chart(ctx1, {
        type: 'line',
        data: {datasets:[{label:'Position [º] or Velocity [rad/s] vs time[s]', backgroundColor: 'rgba(255, 99, 132, 0.2)', borderColor:'rgba(255, 99, 132)', data:[]}, 
                         {label: 'Step Input', backgroundColor: 'rgba(75, 192, 192, 0.2)', borderColor:'rgba(75, 192, 192)', data:[]}]},
        options: {responsive:false}
      });
  }
  
  else{
      lineChart1 = new Chart(ctx1, {
        type: 'line',
        data: {datasets:[{label:'Position [º] or Velocity [rad/s] vs time[s]', backgroundColor: 'rgba(255, 99, 132, 0.2)', borderColor:'rgba(255, 99, 132)', data:[]}, 
                        {label: 'Step Input', backgroundColor: 'rgba(75, 192, 192, 0.2)', borderColor:'rgba(75, 192, 192)', data:[]}]},
        options: {responsive:false}
      });    
    
    }
    
  if (lineChart2!=null){ lineChart2.destroy();
      lineChart2 = new Chart(ctx2, {
        type: 'line',
        data: {labels:[], datasets:[{label:'Control signal', backgroundColor: 'rgba(255, 205, 86, 0.2)', borderColor:'rgba(255, 205, 86)', data:[]}]},
        options: {responsive:false}
      });
  }
  
  else{
      lineChart2 = new Chart(ctx2, {
        type: 'line',
        data: {labels:[], datasets:[{label:'Control signal', backgroundColor: 'rgba(255, 205, 86, 0.2)', borderColor:'rgba(255, 205, 86)', data:[]}]},
        options: {responsive:false}
      });
  }
}

function refresh() {
    positionSlider.value="180";
    velocitySlider.value="0.5";

    document.getElementById("demoPosition").innerHTML = "180";
    document.getElementById("demoVelocity").innerHTML = "3";
    
    velocityStatus=false;
    velocityB.style.backgroundColor="grey";
    
    positionStatus=false;
    positionB.style.backgroundColor="grey";
    
    
    if(selectLoop.value=="open"){
      createGraph()
      $('#ChartC').hide();
    }
    
    else {
      createGraph();
    }
  }
        
$(document).ready(function() {
  $(window).resize();
    
  var socket = io();
  createGraph();
  
  $("#link").hide();
  $('#positionButton').hide();
  $('#velocityButton').hide();
  $('#positionSlider').hide();
  $('#velocitySlider').hide();
  $('#textPosition').hide();
  $('#textVelocity').hide();
  $('#textPID').hide();
  $('#kText').hide();
  $('#graph2Text').hide();
  
  selectLoop.onchange = function () {
 
      refresh();
      $("#link").hide();
      if (selectLoop.value=="open") {
       console.log("open");
        
       $('#positionButton').hide();
       $('#velocityButton').hide();
       $('#positionSlider').hide();
       $('#velocitySlider').hide();
       $('#textPosition').hide();
       $('#textVelocity').hide();
       $('#textPID').hide();
       $('#kText').hide();
       $('#graph2Text').hide();
       
       $('#textDuty').show();
       $('#dutySlider').show();
       $('#velocityStaticButton').show();
      }
        
      if (selectLoop.value=="close") {
       console.log("close");
        
       $('#positionButton').show();
       $('#velocityButton').show();
       $('#positionSlider').show();
       $('#velocitySlider').show();
       $('#textPosition').show();
       $('#textVelocity').show();
       $('#textPID').show();
       $('#kText').show();
       $('#graph2Text').show();
       $('#ChartC').show();
       
       
       $('#textDuty').hide();
       $('#velocityStaticButton').hide();
       $('#dutySlider').hide();
      }
  } 
  
  socket.on('connect', function() { 
    socket.send('User has connected');
  });   

  
  //Data for graphs

  socket.on('dataposition', (time, position)=> {
    time=parseFloat(time).toFixed(3);
    lineChart1.data.labels.push(time);
    lineChart1.data.datasets[0].data.push(position);   
    lineChart1.data.datasets[1].data.push(positionSlider.value);  
    lineChart1.update();
  });
  
  socket.on('datavelocity', (time, velocity)=> {
    time=parseFloat(time).toFixed(3);
    lineChart1.data.labels.push(time);
    lineChart1.data.datasets[0].data.push(velocity);
    if (selectLoop.value=="open"){
      lineChart1.data.datasets[1].data.push((dutySlider.value));
    }
    
    else{
      lineChart1.data.datasets[1].data.push(velocitySlider.value);
    }
    
    lineChart1.update();
  });
  
    socket.on('controlsignal', (time, control)=> {
    time=parseFloat(time).toFixed(3);
    lineChart2.data.labels.push(time);
    lineChart2.data.datasets[0].data.push(control);  
    lineChart2.update();
  });
  
  socket.on('done', ()=> {
    running=0;
    $("#link").show();
    selectLoop.disabled=false;
  });
       
  $("#play").click(function(){
    console.log('Pressed play!')
    if (running==0){
      if (selectLoop.value=="close") {
      
        if ((positionStatus==true) || (velocityStatus==true)){
            
            if (positionStatus==true){
              var controlOption = 0;
              var target = positionSlider.value
            }
            
            if (velocityStatus==true){
              var controlOption = 1;
              var target = velocitySlider.value
            }
            createGraph();
            running=1;
            socket.emit('play_event_close', controlOption, target, kp_value.value, ki_value.value, kd_value.value, sample_time.value, running_time.value); 
        }
      } 
      
      else if (selectLoop.value=="open") {
        createGraph();
        running=1;
        selectLoop.disabled=true;
        socket.emit('play_event_open',dutySlider.value, sample_time.value, running_time.value);
      }
      $("#link").hide();
    }
  });
  
  $("#refresh").click(function(){
    $("#link").hide();
    selectLoop.disabled=false;
    refresh();
  });   
    
  $("#cameraCheck").click(function(){
      console.log(camera_switch);
      if (camera_switch == true) {
        camera_switch = false;
        var cameratodo=document.getElementById("cameraobj")
        cameratodo.src="../static/camera.png"
      }
        
      else {
        camera_switch = true;
        var cameratodo=document.getElementById("cameraobj")
        cameratodo.src="/video_feed"
      }
      console.log(camera_switch);
      socket.emit('camera_switch');
    });
});
