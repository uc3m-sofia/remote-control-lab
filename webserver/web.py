'''
	Design and development of an
	interface for the remote control of a DC motor

	Universidad Carlos III
	Author: Alicia Martín Díaz
	NIA: 100367207

	Web server script
'''
from flask import Flask, render_template, Response
from flask_socketio import SocketIO, emit, send

import RPi.GPIO as GPIO
import cv2
import subprocess
import math
import decimal
import csv

# video capture object with index 0 to specify which camera
camera = cv2.VideoCapture(0)

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)


CAMERA = 1
def read_csv():
	points=[]
	with open("/home/pi/remote-control-lab/webserver/static/data.csv") as f:
		reader= csv.reader(f)
		for row in reader:
			points.append(row)
		return points
	
def main_control_close(_option, _target, _kp, _ki, _kd, _st, _running_time):
	option=float(_option)
	target=math.radians(float(_target))
	running_time = float(_running_time)
	print("Target: ", _target, "Kp: ", _kp, "Ki: ", _ki, "Kd: ", _kd);
	if option == 0:
		# POSITION
		p=subprocess.run(['sudo','/home/pi/Documents/proyectoRPImotorDC/build-c_test-Desktop-Default/SistemaBucleCerradoPos',_st,str(target),_running_time,_kp,_ki,_kd])
		
		points= read_csv()
		for i in range(len(points)):
			emit('dataposition',(str(points[i][0]),str(points[i][1])))
			emit('controlsignal',(str(points[i][0]),str(points[i][2])))
	elif option == 1:
		# VELOCITY
		p=subprocess.run(['sudo','/home/pi/Documents/proyectoRPImotorDC/build-c_test-Desktop-Default/SistemaBucleCerradoVel',_st,_target,_running_time,_kp,_ki,_kd])
		points= read_csv()
		for i in range(len(points)):
			emit('datavelocity',(str(points[i][0]),str(points[i][1])))
			emit('controlsignal',(str(points[i][0]),str(points[i][2])))
	finishAlert()


def main_control_open(_duty, _st, _running_time):
	p=subprocess.run(['sudo','/home/pi/Documents/proyectoRPImotorDC/build-c_test-Desktop-Default/SistemaBucleAbierto',_st,_duty,_running_time])
	points= read_csv()
	for i in range(len(points)):
		socketio.emit('datavelocity',(str(points[i][0]),str(points[i][1])))
	finishAlert()
def gen():
	global CAMARA
	while True:
		# print("The camera state is:"+str(CAMERA))
		ret, frame = camera.read() #if frame is read correctly, ret is True 
		cv2.imwrite('t.jpg', frame) #cv2.imwrite(filename, image)
		yield (b'--frame\r\n'b'Content-Type: image/jpeg\r\n\r\n' + open('t.jpg', 'rb').read() + b'\r\n')



app = Flask(__name__, template_folder='templates')
app.config['SEND_FILE_MAX_AGE_DEFAULT']=0

app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


@app.route("/")
def index():
    return render_template("main.html")

@app.route('/video_feed')
def video_feed():
    """Video streaming route. Put this in the src attribute of an img tag."""
    """<img src="{{ url_for('video_feed') }}" width="400" height="300" alt="image" id="camera">"""
    return Response(gen(), mimetype='multipart/x-mixed-replace; boundary=frame')

   
@socketio.on('message')
def handleMessage(msg):
	global CAMARA
	CAMARA=1
	print('Message: ' + msg)  
	emit('ON', msg)
	
@socketio.on('finish')
def finishAlert():
	emit('done')
	
@socketio.on('camera_switch')
def turncamera():
	global CAMERA
	CAMERA= not CAMERA
	print("The camera state is:"+str(CAMERA))
	if CAMERA == 1:
		video_feed()
		
@socketio.on('play_event_close')
def play(_option, _target, _kp, _ki, _kd, _st, _running_time):
	print("Control Option: ", _option, "Target: ", _target, "Kp: ", _kp, "Ki: ", _ki, "Kd: ", _kd, "St: ", _st, "Running_time", _running_time)
	main_control_close(_option, _target, _kp, _ki, _kd, _st, _running_time)
	
@socketio.on('play_event_open')
def play(_duty, _st, _running_time):
	print("Duty: ", _duty, "St: ", _st, "Running_time", _running_time)
	main_control_open(_duty, _st, _running_time)

	
	
if __name__ == '__main__':
	socketio.run(app, host='0.0.0.0',port=4997) 
    
